var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var kanalGeslo = {};
var grdeBesede  = [];


exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    napolniTabeloGrdihBesed(socket);
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', '');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajPosredovanjeZasebnegaSporocila(socket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    socket.on('uporabniki', function() {
      var kanal = trenutniKanal[socket.id];
      var uporabnikiNaKanalu = io.sockets.clients(kanal);
      var seznamUporabnikov = [];
      for (var i in uporabnikiNaKanalu) {
        var uporabnikId = uporabnikiNaKanalu[i].id;
        seznamUporabnikov.push(vzdevkiGledeNaSocket[uporabnikId]);
      }
      socket.emit('uporabniki', seznamUporabnikov);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};

function napolniTabeloGrdihBesed(socket) {
  if(grdeBesede.length <= 0){
    var fs = require("fs");
    grdeBesede = fs.readFileSync('public/SwearWords.txt', 'utf8').split("\n");
  }
  posredujTabelo(socket, grdeBesede);
}

function posredujTabelo(socket, grdeBesede){
  socket.emit('dobiGrdeBesede', grdeBesede);
}

function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}


function pridruzitevKanalu(socket, kanal, geslo) {
  var uspesno = false;
  if (geslo == '' && kanalGeslo[kanal] == null) {
    socket.join(kanal);
    trenutniKanal[socket.id] = kanal;
    kanalGeslo[kanal] = null;
    uspesno = true;
  } else {
    if (kanalGeslo.hasOwnProperty(kanal)){
      if (geslo == kanalGeslo[kanal]){
        socket.join(kanal);
        trenutniKanal[socket.id] = kanal;
        uspesno = true;
      }
    } else {
      kanalGeslo[kanal] = geslo;
      socket.join(kanal);
      trenutniKanal[socket.id] = kanal;
      
      uspesno = true;
    }
  }
  if (uspesno){
    socket.emit('pridruzitevOdgovor', {
      kanal: kanal,
      vzdevek : vzdevkiGledeNaSocket[socket.id],
      uspesnost: uspesno,
    });
    socket.broadcast.to(kanal).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.'
    });
      
    var uporabnikiNaKanalu = io.sockets.clients(kanal);
    if (uporabnikiNaKanalu.length > 1) {
      var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
      for (var i in uporabnikiNaKanalu) {
        var uporabnikSocketId = uporabnikiNaKanalu[i].id;
        if (uporabnikSocketId != socket.id) {
          if (i > 0) {
            uporabnikiNaKanaluPovzetek += ', ';
          }
          uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
        }
      }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek});}
    for (i in kanalGeslo){
      if (io.sockets.clients(i).length <= 0){
        delete kanalGeslo[i];
      }
    }
  } else {
    socket.join(trenutniKanal[socket.id]);
    socket.emit('pridruzitevOdgovor', {
      kanal: kanal,
      uspesnost: uspesno,
    });
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id]
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.'
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo
    });
  });
}

function dodajSmeske(sporocilo){
  sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"></img>');
  sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></img>');
  sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"></img>');
  sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"></img>');
  sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"></img>');
  
  return sporocilo;
}

function GrdeBesede(spr){
  for (var i = 0; i < grdeBesede.length; i++){
    var regex = new RegExp("\\b" + grdeBesede[i] + "\\b", "gi");
    spr = spr.replace(regex, dodajZvezdice(grdeBesede[i].length));
  }
  return spr;
}

function dodajZvezdice(dol){
  var zvezdice = '';
  for (var i = 0; i < dol; i++){
    zvezdice += '*';
  }
  return zvezdice;
}

function obdelajPosredovanjeZasebnegaSporocila(socket){
  socket.on('zasebnoSporocilo', function(sporocilo) {
    var besedilo = sporocilo.besedilo;
    besedilo = besedilo.replace(/</g, "&lt");
    besedilo = besedilo.replace(/>/g, "&gt");
    besedilo = dodajSmeske(besedilo);
    besedilo = GrdeBesede(besedilo);
    
    if (uporabljeniVzdevki.indexOf(sporocilo.uporabnik) == -1) {
        socket.emit('zasebnoOdgovor', {
        uspesno: false,
        besedilo: 'Sporočila: "' + besedilo + '" uporabniku z vzdevkom "' + sporocilo.uporabnik +'" ni bilo mogoče posredovati. Uporabnik z vzdevkom ' + sporocilo.uporabnik + ' ne obstaja.'
      });
      } else if (vzdevkiGledeNaSocket[socket.id] == sporocilo.uporabnik) {
        socket.emit('zasebnoOdgovor', {
        uspesno: false,
        besedilo: 'Sporočila: "' + besedilo + '" uporabniku z vzdevkom: "' + sporocilo.uporabnik +'" ni bilo mogoče posredovati. Zasebnega sporočila ne morete poslati sebi.'
      });
      } else {
        var kanali = io.sockets.manager.rooms;
        var prejemnik;
       
        for (var kanal in kanali){
          kanal = kanal.substring(1, kanal.length);
          var uporabniki = io.sockets.clients(kanal);
          for (var i in uporabniki){
            var uporabnikId = uporabniki[i].id;
            if (vzdevkiGledeNaSocket[uporabnikId] == sporocilo.uporabnik){
             prejemnik = uporabnikId;
            }
          }
        }
        
        io.sockets.sockets[prejemnik].emit('sporocilo', {
          besedilo: vzdevkiGledeNaSocket[socket.id] + ' (zasebno): ' + besedilo
        });
        socket.emit('zasebnoOdgovor', {
        uspesno: true,
        besedilo: '(zasebno za ' + sporocilo.uporabnik + '): ' + besedilo
      });
      }
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
      socket.leave(trenutniKanal[socket.id]);
      pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
  });
}

function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}