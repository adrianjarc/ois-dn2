var tabelaGrdihBesed = [];

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function dodajSmeske(sporocilo){
  sporocilo = sporocilo.replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png"></img>');
  sporocilo = sporocilo.replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png"></img>');
  sporocilo = sporocilo.replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png"></img>');
  sporocilo = sporocilo.replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png"></img>');
  sporocilo = sporocilo.replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png"></img>');
  
  return sporocilo;
}

function zamenjajGrdeBesede(spr){
  for (var i = 0; i < tabelaGrdihBesed.length; i++){
    var regex = new RegExp("\\b" + tabelaGrdihBesed[i] + "\\b", "gi");
    spr = spr.replace(regex, dodajZvezdice(tabelaGrdihBesed[i].length));
  }
  return spr;
}

function dodajZvezdice(dol){
  var zvezdice = '';
  for (var i = 0; i < dol; i++){
    zvezdice += '*';
  }
  return zvezdice;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }

  } else{
    sporocilo = sporocilo.replace(/</g, "&lt");
    sporocilo = sporocilo.replace(/>/g, "&gt");
    sporocilo = dodajSmeske(sporocilo);
    sporocilo = zamenjajGrdeBesede(sporocilo);
    
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  } 
  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  socket.on('dobiGrdeBesede', function(grdeBesede){
    tabelaGrdihBesed = grdeBesede;
  });

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    if (rezultat.kanal != undefined){
      $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  socket.on('zasebnoOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = $('<div style="font-weight: bold"></div>').html(rezultat.besedilo);
      $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    } else {
      sporocilo = rezultat.besedilo;
      $('#sporocila').append(divElementHtmlTekst(sporocilo));
    }
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    if (rezultat.uspesnost){
      $('#kanal').text(rezultat.vzdevek + ' @ ' + rezultat.kanal);
      $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    } else {
      $('#sporocila').append(divElementHtmlTekst('Napacno geslo za kanal ' + rezultat.kanal + '.'));
    }
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(novElement);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();

    for(var i in uporabniki) {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
      }
    });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);
  
  setInterval(function() {
    socket.emit('uporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});