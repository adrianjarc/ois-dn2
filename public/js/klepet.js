var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.posljiZasebnoSporocilo = function(uporabnik, besedilo) {
  var sporocilo = {
    uporabnik: uporabnik,
    besedilo: besedilo
  };
  this.socket.emit('zasebnoSporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      var kanal;
      besede.shift();
      if (besede[0].indexOf("\"") != 0){
        kanal = besede.join(' ');
        this.spremeniKanal(kanal, '');
        break;
      } else {
        if (besede.length == 1){
          kanal = besede.join(' ');
          this.spremeniKanal(kanal, '');
          break;
        } else {
          var pomoc = besede.join(' ');
          var pomoc2 = pomoc.split('"');
          pomoc2.shift();
          kanal = pomoc2[0];
          pomoc2.shift();
          var geslo = pomoc2.join('');
          geslo = geslo.substring(1,geslo.length);
          this.spremeniKanal(kanal, geslo);
          break;
        }
      }
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      if(besede.length > 2){
        besede.shift();
        if (besede[0].charAt(0) == "\"" && besede[0].charAt(besede[0].length-1) == "\""){
          var uporabnik = besede[0].substring(1, besede[0].length-1);
          besede.shift();
        } else {
          sporocilo = 'Nepravilno formatiranje ukaza pozabili ste ".';
        break;
        }
        var spr = besede.join(' ');
        if (spr.charAt(0) == "\"" && spr.charAt(spr.length-1) == "\""){
          this.posljiZasebnoSporocilo(uporabnik, spr.substring(1, spr.length-1));
          break;
        } else {
          sporocilo = 'Nepravilno formatiranje ukaza pozabili ste ".';
        break;
        }
        
      } else {
        sporocilo = 'Neznan ukaz.';
        break;
      }
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};